FROM openjdk:8-jre
MAINTAINER Nicolas SVIRCHEVSKY <n.svirchevsky@gmail.com>

ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

WORKDIR /parkever
ENTRYPOINT ["java", "-jar", "/usr/share/parkever/parkever.jar"]

ARG JAR_FILE
ADD target/${JAR_FILE} /usr/share/parkever/parkever.jar