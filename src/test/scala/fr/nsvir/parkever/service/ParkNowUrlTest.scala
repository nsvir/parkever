package fr.nsvir.parkever.service

import org.scalatest.matchers.should.Matchers

class ParkNowUrlTest extends org.scalatest.funsuite.AnyFunSuite with Matchers {


  val parkNowUrl: ParkNowUrl = new ParkNowUrl

  test("parkingPrice") {
    val request = ParkingRequest("vehicleVrn",
      "stopTimeLocal",
      "behalfOfUserId",
      "eligibilityProfile",
      "supplierId")
    val actualUri = parkNowUrl.parkingPrice("zipCode", request)

    val expectedUri = "https://webservices.park-now.com/api/parking/price/zipCode?format=json&stopTimeLocal=stopTimeLocal&vehicleVrn=vehicleVrn&eligibilityProfile=eligibilityProfile&supplierId=supplierId&behalfOfUserId=behalfOfUserId"

    actualUri.toString() should equal(expectedUri)
  }

  test("confirmParking") {
    val actionId = 345678
    val expectedUri = s"https://webservices.park-now.com/api/parking/confirm/$actionId?format=json"

    val actualUri = parkNowUrl.confirmParking(actionId)

    actualUri.toString() should equal(expectedUri)
  }


  test("active parkings") {
    val expectedUri = s"https://webservices.park-now.com/api/parking/active?format=json"

    val actualUri = parkNowUrl.activeParking()

    actualUri.toString() should equal(expectedUri)
  }

}
