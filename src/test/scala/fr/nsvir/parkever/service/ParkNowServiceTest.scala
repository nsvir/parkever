package fr.nsvir.parkever.service

import fr.nsvir.parkever.util.Time
import org.joda.time.DateTime
import org.scalatest.EitherValues
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import sttp.client.Identity
import sttp.client.testing.SttpBackendStub

class ParkNowServiceTest extends AnyFunSuite with Matchers with EitherValues {

  private val sttpBackendStub: SttpBackendStub[Identity, Nothing, Nothing] = SttpBackendStub.synchronous

  def parkNowApi[T](backend : SttpBackendStub[Identity, Nothing, Nothing])(function: ParkNowService => T) : T = {
    function(new ParkNowService {
      override protected lazy val basicAuthRequest = new BasicAuthRequest()(backend)
    })
  }

  test("GET parking price, returns parkingActionId if parking isParkingAllowed is true") {
    val expectedParkingActionId = 24133658
    val configuredBackend = sttpBackendStub.whenAnyRequest
        .thenRespond(s"""{"isParkingAllowed":true, "parkingActionId": $expectedParkingActionId}""".stripMargin)

    val result = parkNowApi(configuredBackend)(_.getParkingActionId)

    assertResult(Right(expectedParkingActionId))(result)
  }

  test("GET parking price, returns left if invalid JSON") {
    val configuredBackend = sttpBackendStub.whenAnyRequest
      .thenRespond(s"""invalid json""".stripMargin)

    val result = parkNowApi(configuredBackend)(_.getParkingActionId)

    assert(result.isLeft)
  }

  test("GET parking price, returns left if isParkingAllowed is missing") {
    val configuredBackend = sttpBackendStub.whenAnyRequest
      .thenRespond(s"""{}""".stripMargin)

    val result = parkNowApi(configuredBackend)(_.getParkingActionId)

    assert(result.isLeft)
  }

  test("GET parking price, returns left(parkingNotAllowedReason) if parking isParkingAllowed is false") {
    val expectedReason = "Some Reason"
    val configuredBackend = sttpBackendStub.whenAnyRequest
      .thenRespond(s"""{"isParkingAllowed": false, "parkingNotAllowedReason": "$expectedReason" }""")

    val result = parkNowApi(configuredBackend)(_.getParkingActionId)

    result.left.value should include (s""""$expectedReason"""")
  }

  test("GET parking price, returns left if parking isParkingAllowed is false and missing parkingNotAllowedReason") {
    val configuredBackend = sttpBackendStub.whenAnyRequest
      .thenRespond(s"""{"isParkingAllowed": false}""")

    val result = parkNowApi(configuredBackend)(_.getParkingActionId)

    assert(result.isLeft)
  }

  test("GET parking price, returns left if parking isParkingAllowed is not a boolean") {
    val configuredBackend = sttpBackendStub.whenAnyRequest
      .thenRespond(s"""{"isParkingAllowed": "not a boolean but a string"}""")

    val result = parkNowApi(configuredBackend)(_.getParkingActionId)

    assert(result.isLeft)
  }

  test("GET parking price, returns left if parking isParkingAllowed is true & parkingActionId is missing") {
    val configuredBackend = sttpBackendStub.whenAnyRequest
      .thenRespond(s"""{"isParkingAllowed": true}""")

    val result = parkNowApi(configuredBackend)(_.getParkingActionId)

    assert(result.isLeft)
  }

  test("GET parking price, returns left if parking isParkingAllowed is true & parkingActionId is not a number") {
    val configuredBackend = sttpBackendStub.whenAnyRequest
      .thenRespond(s"""{"isParkingAllowed": true, "parkingActionId": "not a number}""")

    val result = parkNowApi(configuredBackend)(_.getParkingActionId)

    assert(result.isLeft)
  }

  test("GET active parking with 1 parking") {
    val dateTime = Time.fromIso("2020-07-11T12:52:57").get

    val configuredBackend = sttpBackendStub.whenAnyRequest
      .thenRespond(s"""{"actions": [{"stopLocal": "2020-07-11T12:52:57"}]}""")

    val result = parkNowApi(configuredBackend)(_.getStopParking)

    result should equal(Right(dateTime))
  }

  test("GET active parking with 0 parking") {
    val dateTime = Time.fromIso("2020-07-11T12:52:57").get

    val configuredBackend = sttpBackendStub.whenAnyRequest
      .thenRespond(s"""{"actions": []}""")

    val result = parkNowApi(configuredBackend)(_.getStopParking)

    result shouldBe a [Left[_, _]]
  }

  test("GET active parking with more than 1 parking") {
    val configuredBackend = sttpBackendStub.whenAnyRequest
      .thenRespond(s"""{"actions": [{"stopLocal": "2020-07-11T12:52:57"}, {"stopLocal": "2020-07-11T12:52:57"}]}""")

    val result = parkNowApi(configuredBackend)(_.getStopParking)

    result shouldBe a [Left[_, _]]
  }

  test("POST confirm minimal") {
    val configuredBackend = sttpBackendStub.whenAnyRequest
      .thenRespond("""{"parkingActionId":24819314, "actions":[{"stopLocal":"2020-07-14T12:53:23"}]}""")

    val result = parkNowApi(configuredBackend)(_.confirmParkingAction(24819314))

    result shouldBe a [Right[_, _]]
  }
}
