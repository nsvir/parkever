package fr.nsvir.parkever.util

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

case class Person(name : String, age: Int, major : Boolean)

class ProductsTest extends AnyFunSuite with Matchers {

  test("AnyMap valid case class") {
    val resultMap = Products.toMap(Person("Nicolas", 99, true))

    val expectedMap = Map("name" -> "Nicolas", "age" -> "99", "major" -> "true")

    resultMap should contain allElementsOf expectedMap
  }
}
