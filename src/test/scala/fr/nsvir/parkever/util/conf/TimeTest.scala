package fr.nsvir.parkever.util.conf


import fr.nsvir.parkever.util.Time
import org.joda.time.{DateTime, DateTimeZone, Instant}
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

import scala.util.Success

class TimeTest extends AnyFunSuite with Matchers {

  test("DatetimeString to dateTime") {
    val expected = new DateTime(2020,7, 13, 12, 52, 57)
      .withZone(DateTimeZone.forOffsetHours(2))

    val actual = Time.fromIso("2020-07-13T12:52:57")

    actual should equal(Success(expected))
  }

  test("TimeIsForward") {
    Time.forward(30) should be > Instant.now.toDateTime
  }

  test("Time format is epoch") {
    val date = new DateTime(2020, 7, 5, 16, 30, 5, 0, DateTimeZone.forOffsetHours(0))
    val expectedDate = "2020-07-05T18:30:05.000Z"
    Time.formatIso(date) should equal(expectedDate)
  }
}
