package fr.nsvir.parkever.util

object Products {

  def toMap(product: Product): Map[String, String] = {
    val fields = product.getClass.getDeclaredFields.map(_.getName)
    val values = product.productIterator.to(List)
    for {
      (k, v) <- fields.zip(values).toMap
    } yield k -> v.toString
  }

}
