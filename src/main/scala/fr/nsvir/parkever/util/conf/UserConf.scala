package fr.nsvir.parkever.util.conf

case class UserConf(basicAuth: String,
                    vehicleVrn: String,
                    userId: String,
                    supplierId: String,
                    eligibilityProfile: String,
                    zipCode: String)
