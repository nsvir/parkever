package fr.nsvir.parkever.util

import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import org.joda.time.{DateTime, DateTimeZone, Instant}

import scala.util.Try

object Time {

  def fromIso(dateTimeString: String): Try[DateTime] = {
    val format: DateTimeFormatter = DateTimeFormat
      .forPattern("yyy-MM-dd'T'HH:mm:ss")
      .withZone(DateTimeZone.forOffsetHours(2))
    Try(format.parseDateTime(dateTimeString))
  }

  def forward(minutes: Int): DateTime = Instant.now.toDateTime.plusMinutes(minutes)

  def formatIso(dateTime: DateTime): String = {
    val format: DateTimeFormatter = DateTimeFormat
      .forPattern("yyy-MM-dd'T'HH:mm:ss.SSS'Z'")
      .withZone(DateTimeZone.forOffsetHours(2))
    dateTime.toString(format)
  }
}
