package fr.nsvir.parkever


import fr.nsvir.parkever.service.ParkNowService
import org.apache.logging.log4j.LogManager
import org.joda.time.{DateTime, Instant, Minutes}
import scalaz.{-\/, \/-}
import scalaz.concurrent.Task

import scala.concurrent.duration._

object Application extends App {

  lazy private val log = LogManager.getLogger(Application)

  val backoff = FiniteDuration(30, SECONDS)
  val attempt = 3
  var attemptCount = 0

  val parkNowApi = new ParkNowService()

  @scala.annotation.tailrec
  def repeatedTask(action: () => Duration, maybeDuration: Option[Duration] = None) : Unit = {
    val duration: Duration = maybeDuration.getOrElse(action())
    log.info(s"Duration before next execution: $duration\n")
    Task.schedule(action(), duration).unsafePerformSyncAttempt match {
      case \/-(duration) =>
        attemptCount = 0
        repeatedTask(action, Some(duration))
      case -\/(exception) =>
        if (attemptCount >= attempt) {
          log.info(s"Attempted $attemptCount times to proceed, program is shutting down..")
        } else {
          attemptCount += 1
          log.info(s"""Something wrong happened : "$exception", attempt to proceed number : $attemptCount""")
          repeatedTask(action, Some(backoff))
        }
    }
  }

  lazy val activeParkingWhenStopReached = () => {
    log.info("Getting next parking activity stop")
    parkNowApi.getStopParking.map { stopDateTime =>
      log.info(s"Found: $stopDateTime ")
      getFiniteDurationFromNow(stopDateTime)
    } match {
      case Right(duration) => duration
      case Left(error) =>
        log.info(s"Not found :$error")
        log.info("Activating parking session")
        activateParking match {
          case Right(newParkingStop) =>
            log.info(s"""Parking sessions activated until: "$newParkingStop"""")
            getFiniteDurationFromNow(newParkingStop)
          case Left(error) =>
            log.error(s"""Cannot activate parking session: "$error" """)
            throw new RuntimeException(error)
        }
    }
  }

  repeatedTask(activeParkingWhenStopReached, Some(FiniteDuration(1, SECONDS)))

  private def getFiniteDurationFromNow(stopDateTime: DateTime) = {
    FiniteDuration(Minutes.minutesBetween(Instant.now, stopDateTime.toInstant).getMinutes + 1, MINUTES)
  }

  def activateParking: Either[String, DateTime] = {
    for {

      actionId <- {
        log.info("Getting parking actionId")
        parkNowApi.getParkingActionId
      }

      stopLocal <- {
        log.info(s"""ActionId : $actionId""")
        log.info(s"""Confirming parking action""")
        parkNowApi.confirmParkingAction(actionId)
      }

    } yield stopLocal
  }

}
