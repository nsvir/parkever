package fr.nsvir.parkever.service

import java.nio.file.{Files, Paths}

import fr.nsvir.parkever.util.conf.UserConf
import pureconfig.ConfigSource
import pureconfig.generic.auto._

class ConfigService {

  private val defaultExternalConfigurationFile = "application.conf"

  private lazy val configurationSource = {
    if (Files.exists(Paths.get(defaultExternalConfigurationFile))) {
      ConfigSource.file(defaultExternalConfigurationFile)
    } else {
      ConfigSource.default
    }
  }

  lazy val userConf: UserConf = {
    configurationSource.load[UserConf] match {
      case Right(userConf) => userConf
      case Left(failure) => throw new RuntimeException(s"Failed to load configuration : ${failure.prettyPrint()}")
    }
  }

}
