package fr.nsvir.parkever.service

import com.softwaremill.macwire.wire
import io.circe._
import sttp.client.circe._
import sttp.client.{HttpURLConnectionBackend, Identity, NothingT, SttpBackend, _}
import sttp.model.{Header, Uri}
import io.circe.syntax._

class BasicAuthRequest(implicit backend: SttpBackend[Identity, Nothing, NothingT] = HttpURLConnectionBackend()) {

  private lazy val userConf = wire[ConfigService].userConf

  private val Authorization = Header("Authorization", s"Basic ${userConf.basicAuth}")
  private val JsonContentType = Header("Content-Type", "application/json")
  private val AcceptEncoding = Header("Accept-Encoding", "gzip, deflate")

  val baseRequest: RequestT[Empty, Either[String, String], Nothing] = emptyRequest.headers(Authorization, JsonContentType, AcceptEncoding)

  def get[T: Decoder : IsOption](url: Uri): Either[String, T] = {
    val response = baseRequest
      .get(url)
      .response(asJson[T])
      .send()

    handleBodyResponse(response)
  }

  def post[A: Encoder, T: Decoder : IsOption](url: Uri, payload: A): Either[String, T] = {
    val response = baseRequest
      .post(url)
      .body(payload.asJson)
      .response(asJson[T])
      .send()

    handleBodyResponse(response)
  }

  private def handleBodyResponse[T: Decoder : IsOption](response: Identity[Response[Either[ResponseError[Error], T]]]): Either[String, T] = {
    response.body match {
      case Right(content) => Right(content)
      case Left(HttpError(body)) => Left(s"Http error (${response.statusText}) : $body")
      case Left(DeserializationError(body, error)) => Left(s"""Body response error  : "$error" in "$body"""")
    }
  }
}
