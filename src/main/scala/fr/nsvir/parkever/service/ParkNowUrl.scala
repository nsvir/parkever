package fr.nsvir.parkever.service

import fr.nsvir.parkever.util.Products
import sttp.client._
import sttp.model.Uri

class ParkNowUrl {

  private val parkNowBaseUrl = "https://webservices.park-now.com"

  private def parkNowQuery(apiUrl: String, queryString: Map[String, String]) = {
    val url = s"$parkNowBaseUrl/$apiUrl"
    uri"$url?$queryString"
  }

  def parkingPrice(zipCode: String, parkingRequest: ParkingRequest): Uri = {
    val parkingRequestMap = Products
      .toMap(parkingRequest)
      .updated("format", "json")

    parkNowQuery(s"api/parking/price/$zipCode", parkingRequestMap)
  }

  def confirmParking(actionId: Long): Uri = {
    parkNowQuery(s"api/parking/confirm/$actionId", Map("format" -> "json"))
  }

  def activeParking(): Uri = {
    parkNowQuery(s"api/parking/active", Map("format" -> "json"))
  }

}

case class ParkingRequest(vehicleVrn: String,
                          stopTimeLocal: String,
                          behalfOfUserId: String,
                          eligibilityProfile: String,
                          supplierId: String)

case class ConfirmationBody()
