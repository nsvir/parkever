package fr.nsvir.parkever.service

case class Action(stopLocal: String)

case class ActiveParkingResponse(actions: Seq[Action])
