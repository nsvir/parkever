package fr.nsvir.parkever.service

import com.softwaremill.macwire._
import fr.nsvir.parkever.util.Time
import fr.nsvir.parkever.util.conf.UserConf
import io.circe.generic.auto._
import org.joda.time.DateTime

class ParkNowService {

  protected lazy val userConf: UserConf = wire[ConfigService].userConf
  private lazy val parkNowUrl: ParkNowUrl = wire[ParkNowUrl]
  protected lazy val basicAuthRequest: BasicAuthRequest = wire[BasicAuthRequest]

  case class ParkingPriceResponse(isParkingAllowed: Boolean, parkingActionId: Option[Long], parkingNotAllowedReason: Option[String])

  case class ConfirmParkingResponse(parkingActionId: Long, actions: Seq[Action])

  def getStopParking: Either[String, DateTime] = {
    val activeParkingUri = parkNowUrl.activeParking()
    basicAuthRequest
      .get[ActiveParkingResponse](activeParkingUri)
      .flatMap { activeParkingResponse =>
        activeParkingResponse.actions match {
          case action :: Nil => Time.fromIso(action.stopLocal).toEither.left.map(_.toString)
          case Nil => Left("No active parking")
          case _ => Left("More than one active parking")
        }
      }
  }

  def getParkingActionId: Either[String, Long] = {

    val parkingRequest = ParkingRequest(userConf.vehicleVrn,
      Time.formatIso(Time.forward(30)),
      userConf.userId,
      userConf.eligibilityProfile,
      userConf.supplierId)
    val parkingPriceUri = parkNowUrl.parkingPrice(userConf.zipCode, parkingRequest)

    basicAuthRequest
      .get[ParkingPriceResponse](parkingPriceUri)
      .flatMap { content =>
        if (content.isParkingAllowed) {
          content.parkingActionId match {
            case Some(parkingActionId) => Right(parkingActionId)
            case None => Left(s"""Missing or invalid "parkingActionId" in "$content"""")
          }
        } else {
          val reason = content.parkingNotAllowedReason.getOrElse("No parkingNotAllowedReason")
          Left(s"""Parking not allowed : "$reason"""")
        }
      }
  }

  def confirmParkingAction(actionId: Long): Either[String, DateTime] = {
    val confirmParkingActionUri = parkNowUrl.confirmParking(actionId)

    basicAuthRequest
      .post[ConfirmationBody, ConfirmParkingResponse](confirmParkingActionUri, ConfirmationBody() )
      .flatMap { confirmation : ConfirmParkingResponse =>
        if (confirmation.parkingActionId != actionId) {
          Left(s"""Error : parkingActionIds doesn't match expected "$actionId", actual "${confirmation.parkingActionId}""")
        } else {
          val stopLocal = confirmation.actions match {
            case action :: Nil => Right(action.stopLocal)
            case Nil => Left(s"""Missing action in confirmationResponse "$confirmation"""")
            case _ => Left(s"""More than one action in confirmationResponse "$confirmation"""")
          }
          stopLocal.flatMap { Time.fromIso(_).toEither.left.map(_.toString) }
        }
      }
  }
}
